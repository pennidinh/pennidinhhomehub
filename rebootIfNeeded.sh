#!/usr/bin/env bash

sleep $((RANDOM % 5))
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

checkResult=$(curl https://shouldreboot.api.pennidinh.com/check --data-binary "@.env" 2>/dev/null)
echo "$checkResult"
if [ "$checkResult" == "\"yes\"" ]; then
    echo "Reboot"
    reboot
else
    echo "Do not Reboot"
fi
