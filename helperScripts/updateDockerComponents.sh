docker system prune -f --volumes
docker network prune -f
/usr/local/bin/docker-compose pull --no-parallel
/usr/local/bin/docker-compose up -d --remove-orphans
