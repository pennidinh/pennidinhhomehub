INTERNAL_IP=`/sbin/ifconfig | grep "eth" -A 1 | grep -oP "((?<=inet )|(?<=inet addr:))(172|10|100|192|198)\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"`
if [ ! -z "$INTERNAL_IP" ]
then
  grep -q '^INTERNAL_IP_ADDRESS=' .env  && sed -i "s/INTERNAL_IP_ADDRESS=.*/INTERNAL_IP_ADDRESS=$INTERNAL_IP/" .env || echo "INTERNAL_IP_ADDRESS=$INTERNAL_IP" >> .env
fi
