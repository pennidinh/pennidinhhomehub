#!/usr/bin/env bash

sleep $((RANDOM % 60))
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"
git pull --rebase

./helperScripts/reportDiagnostics.sh
./helperScripts/updateLocalIpAddress.sh
./helperScripts/updateDockerComponents.sh
